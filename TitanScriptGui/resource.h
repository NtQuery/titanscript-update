//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TitanScriptGui.rc
//
#define IDC_MYICON                      2
#define IDD_TITANSCRIPTGUI_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDD_MAINWINDOW                  129
#define IDI_ICON1                       130
#define IDC_RUN                         1000
#define IDC_TARGETPATH                  1001
#define IDC_SCRIPTPATH                  1002
#define IDC_BROWSETARGET                1003
#define IDC_BROWSESCRIPT                1004
#define IDC_LOG                         1005
#define IDC_BUTTON1                     1006
#define IDC_COPY                        1006
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
